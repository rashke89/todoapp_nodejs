const express = require('express'),
    app = express(),
    todoController = require('./controllers/todoController'),
    port = 3000;

//    set up template engine
app.set('view engine', 'ejs');

//     static files
app.use(express.static('./public'));

//  fire controllers
todoController(app);

//  listen to port
app.listen(port);
console.log('Server is running on the port: ' + port)