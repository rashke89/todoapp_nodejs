const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//  connect to db
mongoose.connect('mongodb://todoapp:todoapp@ds137281.mlab.com:37281/todo');

//  create shema - this is like a blueprint
var todoSchema = new mongoose.Schema({
        item: String
    });
//  create a collection
var Todo = mongoose.model('Todo', todoSchema);
var urlencodedParser = bodyParser.urlencoded({extended: false});

module.exports = function(app){

    //  req
    app.get('/todo', function(req, res){
        // get data from mnogodb and pass it to view
        Todo.find({}, function(err, data){
            if(err) throw err;
            //  render view name
            res.render('todo', {todos: data})
        });
    });

    //  insert
    app.post('/todo', urlencodedParser, function(req, res){
        //  get data from view and add it to mongodb
        var newTodo = Todo(req.body).save(function(err, data){
            if (err) throw err;
            res.json(data);
        });
    })

    //  delete
    app.delete('/todo/:item', function(req, res){
        //  delete item from mongoDB
        Todo.find({item:req.params.item.replace(/\-/g, ' ')}).remove(function(err, data){
            if (err) throw err;
            res.json(data);
        })
    })
}










